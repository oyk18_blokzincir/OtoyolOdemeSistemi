pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract OtoyolUcretSistemi{
    struct Arac{
        string plaka;
        address etiketAdresi;
        uint aracBakiyesi;
        uint aracsinifi;
    }
    address KarayollariAdresi= 0xca35b7d915458ef540ade6068dfe2f44e8fa733c;
    Arac[] araclar;
    uint mevcutAracSayisi;
    function odeme(string _plaka) returns(string){
        uint i=0;
        uint[] ucretler;
        ucretler.push(8);
        ucretler.push(11);
        ucretler.push(24);
        ucretler.push(49);
        ucretler.push(65);
        ucretler.push(3);
        for(; i<araclar.length; i++){
            if(keccak256(araclar[i].plaka)==keccak256(_plaka)){
                break;
            }
        }
        require(araclar[i].etiketAdresi.balance>ucretler[araclar[i].aracsinifi-1],
        "Bakiyeniz Yetersiz! Yükleme Yapınız!");
       araclar[i].etiketAdresi.send(ucretler[araclar[i].aracsinifi-1]);
        return "Ödeme Başarılı!";
    }
    function send(uint _ucret) payable {
        KarayollariAdresi.send(_ucret);
    }
    function AracEkle(string _plaka, address _adres, uint _aracsinifi) returns(Arac){
        Arac vasita;
        vasita.plaka=_plaka;
        vasita.etiketAdresi=_adres;
        vasita.aracsinifi=_aracsinifi;
        vasita.aracBakiyesi=20;
        araclar.push(vasita);
        //araclar[mevcutAracSayisi].etiketAdresi.balance;
        return vasita;
    }
}

