Blokzincir Tabanlı Otoyol Ödeme Sistemi

Otoyol ödemelerinde aracı bankaların işlevlerini ortadan kaldırıp üçüncü parti bir otoriteye (bankalar, postaneler vs.) ihtiyaç kalmadan doğrudan eşten eşe ödeme yapılmasını sağlayan sistemdir.